
# Asaba_Fullstack Test Ilmi

## Intro
Aplikasi ini adalah aplikasi optimasi penyimpanan barang dalam gudang. Dibuat dengan Laravel v8.83.27 dengan PHP v7.4.6. Fitur dalam aplikasi ini antara lain adalah : 

 1. Register
 2. Login
 3. Tambah Data Log Gudang
 4. Lihat Data Log Gudang
 5. Lihat Detail Data Log Gudang

Sistem secara otomatis akan menyimpan dan menentukan peletakan tiap item, sesuai jumlah yang diinputkan ke dalam 'rak' dan baris yang paling optimal.

## Cara Menjalankan Aplikasi
 1. Clone repo ini ke pc anda (download / git clone https://gitlab.com/muhammadaliyya19/asaba_fullstack-test-ilmi.git)
 2. Masuk ke folder aplikasi dan silakan buat .env file
 3. Lakukan penyesuaian khususnya pada bagian konfigurasi database, sesuaikan dengan database yang anda gunakan. 
 4. Eksekusi "composer install"
 5. Eksekusi "npm install"
 6. Eksekusi "php artisan key:generate"
 7. Eksekusi "php artisan migrate"
 8. Eksekusi "php artisan serve"
 9. Akses sistem melalui localhost:8000 (atau sesuai konfigurasi port anda)

```

//List perintah eksekusi dalam git bash, powershell, atau vscode terminal

git clone https://gitlab.com/muhammadaliyya19/asaba_fullstack-test-ilmi.git
composer install
npm install
php artisan key:generate
php artisan migrate
php artisan serve

``` 

