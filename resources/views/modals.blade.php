<!-- Modal Add -->
<div class="modal fade" id="addLogModal" tabindex="-1" role="dialog" aria-labelledby="addLogModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Add Log</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_input" class="form" autocomplete="off">
                    @csrf
                    <div class="form-group">
                        <label for="formGroupExampleInput">Sum Items</label>
                        <input type="number" class="form-control" id="jml_masuk" name="jml_masuk" placeholder="Input here...">
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput2">Created By</label>
                        <input type="text" class="form-control" id="created_by" name="created_by" placeholder="Input oleh..." readonly placeholder="Input oleh..." value="{{ $data['nama_user'] }}">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_cm" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" id="btn_calculate" class="btn btn-success">Calculate Needed Space</button>
                <button type="button" id="btn_save" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>        
</div>

<!-- Modal view detail -->
<div class="modal fade bd-example-modal-lg" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Data Log</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Heading info -->
                <div class="row">
                    <div class="col-2 fw-bold">Log Id</div>
                    <div class="col-4">: <span class="log_id"></span></div>
                    <div class="col-2 fw-bold">Created By</div>
                    <div class="col-4">: <span class="created_by"></span></div>
                    
                </div>
                <div class="row mt-2">
                    <div class="col-2 fw-bold">Sum Items</div>
                    <div class="col-4">: <span class="sum_items"></span></div>
                    <div class="col-2 fw-bold">Input Date</div>
                    <div class="col-4">: <span class="created_at"></span></div>
                </div>
                <div class="modal_dtl_body mt-3">
                    <!-- space for table -->
                </div>
            </div>            
        </div>
    </div>
</div>

<!-- Modal view all items -->
<div class="modal fade bd-example-modal-lg" id="allItemModal" tabindex="-1" role="dialog" aria-labelledby="allItemModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">All Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal_all_item">                                
            </div>            
        </div>
    </div>
</div>