@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-left fw-bolder h4">Storage Log</div>
                        <div class="col text-right">
                            <a href="#" class="btn btn-md btn-success btn_all_items" data-toggle="modal" data-target="#allItemModal"> 
                                <i class="fa fa-info-circle"></i> View All Items
                            </a>
                            <!-- Button trigger modal -->                            
                            <a href="#" class="btn btn-md btn-primary btn_add_log" data-toggle="modal" data-target="#addLogModal"> 
                                <i class="fa fa-plus"></i> Add Log
                            </a>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Space Info</h5>
                            <div class="row">
                                <div class="col">
                                    Total Space <!-- Based on rack volume divided items volume -->
                                </div>
                                <div class="col">
                                    : <b><span id="total_space"></span></b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    Maximum Capacity <!-- Based on rack weight capacity -->
                                </div>
                                <div class="col">
                                    : <b><span id="max_capacity"></span></b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    Space Used
                                </div>
                                <div class="col">                            
                                    : <b><span id="space_used"></span></b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    Available Space Remain 
                                </div>
                                <div class="col">                            
                                    : <b><span id="space_remain"></span></b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    Space Unused <!-- Total space minus space used -->
                                </div>
                                <div class="col">                            
                                    : <b><span id="space_unused"></span></b>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h5>Rack Info</h5>
                            <div class="row">
                                <div class="col">
                                    Total Rack
                                </div>
                                <div class="col">                            
                                    : <b><span id="total_rack"></span></b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    Rack Used
                                </div>
                                <div class="col">                            
                                    : <b><span id="rack_used"></span></b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    Rack Remain
                                </div>
                                <div class="col">                            
                                    : <b><span id="rack_remain"></span></b>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <hr>
                    <!-- Datatable -->
                    <div>
                        <table id="dt_log_gudang" class="display" style="width:100%">                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>

@include('modals')
@endsection
@section('script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="{{ asset('js/custom.js') }}"></script>
@endsection
