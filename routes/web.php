<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\GudangController::class, 'index'])->name('home');
Route::get('/gudang/list', [App\Http\Controllers\GudangController::class, 'list']);
Route::post('/gudang/save', [App\Http\Controllers\GudangController::class, 'save']);
Route::get('/gudang/detail/{id}', [App\Http\Controllers\GudangController::class, 'detail']);
Route::get('/gudang/all-items', [App\Http\Controllers\GudangController::class, 'allItem']);
