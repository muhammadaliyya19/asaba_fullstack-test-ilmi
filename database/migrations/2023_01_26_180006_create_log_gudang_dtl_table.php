<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogGudangDtlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_gudang_dtls', function (Blueprint $table) {
            $table->id();
            $table->integer('id_log_gudang');
            $table->integer('no_urut');
            $table->string('rak_simpan');
            $table->string('baris_ke');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_gudang_dtls');
    }
}
