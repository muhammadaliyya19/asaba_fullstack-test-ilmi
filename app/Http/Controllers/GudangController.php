<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\LogGudang;
use App\Models\LogGudangDtl;

class GudangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $req)
    {
        $data = [
            'title'     => 'Warehouse Storage App',
            'nama_user' => \Auth::user()->name
        ];
        return view('home', compact('data'));
    }

    public function list(Request $req)
    {
        $resVal = [];
        $cur_sum = 0;
        $log_gudang = DB::table('log_gudangs')
                    ->join('users', 'log_gudangs.created_by','=', 'users.id')
                    ->select('log_gudangs.*', 'users.name')->get(); 

        foreach ($log_gudang as $lg) {
            $cur_sum += $lg->jml_masuk;
            $lg->current_sum = $cur_sum;
            array_push($resVal, $lg);
        }

        $last_items = LogGudangDtl::orderBy('id', 'desc')->first();

        $head_data = [
            'total_space'   => 237600,
            'max_capacity'  => 5100,
            'space_used'    => $cur_sum,
            'space_remain'  => 5100-$cur_sum,
            'space_unused'  => 237600-$cur_sum,
            'total_rack'    => 100,
            'rack_used'     => $last_items == null ? 0 : $last_items->rak_simpan,
            'rack_remain'   => $last_items == null ? 100 : 100 - $last_items->rak_simpan,
        ];
        return json_encode([
            "data" => $resVal,
            "header" => $head_data
        ]);
    }

    public function save(Request $req)
	{
		if ($req->jml_masuk < 1) {
			echo json_encode([
                'status' => 400,
                'small_info' => 'Invalid amount!'
            ]);
		}else{            
			try {
                $counter_barang = DB::table('log_gudangs')->sum('jml_masuk');
                $total_after = $counter_barang + $req->jml_masuk;
                if ($total_after < 5100) {
                    // Add master data Log Gudang
                    $log_gudang = new LogGudang;
                    $log_gudang->jml_masuk = $req->jml_masuk;
                    $log_gudang->created_by = \Auth::id();
                    $log_gudang->save();
                    $log_gudang_id = $log_gudang->id;

                    for ($i=0; $i < $req->jml_masuk; $i++) { 
                        ++$counter_barang;  
                        
                        // Penetapan rak berdasarkan nomor urut
                        if ($counter_barang % 51 == 0) {
                            $rak_ke     = intdiv($counter_barang, 51);
                        } else {
                            $rak_ke     = intdiv($counter_barang, 51) + 1;
                        }                    
                        
                        // Penetapan baris berdasarkan nomor urut
                        if ($counter_barang % 51 == 0) {
                            $baris_ke = 4;
                        }else if ($counter_barang % 51 <= 18) {
                            $baris_ke = 1;
                        }else if($counter_barang % 51 > 18 && $counter_barang % 51 <= 33){
                            $baris_ke = 2;
                        }else if($counter_barang % 51 > 33 && $counter_barang % 51 <= 45){
                            $baris_ke = 3;                        
                        }else{                        
                            $baris_ke = 4;
                        }
                        
                        $logGudangDtl = new LogGudangDtl;
                        $logGudangDtl->id_log_gudang = $log_gudang_id;
                        $logGudangDtl->no_urut       = $counter_barang;
                        $logGudangDtl->rak_simpan    = $rak_ke;
                        $logGudangDtl->baris_ke      = $baris_ke;
                        $logGudangDtl->created_by    = \Auth::id();
                        $logGudangDtl->save();
                    }                

                    $result = [
                        'status' => 200,
                        'sum_log_added' => 1,
                        'sum_item_added' => $req->jml_masuk,
                        'current_total_item' => $counter_barang
                    ];
                    echo json_encode($result);
                } else {
                    $result = [
                        'status' => 500,
                        'small_info' => 'Out of capacity!',
                        'sisa_space' => (5100 - $counter_barang)
                    ];
                    echo json_encode($result);
                }                            
            } catch (\Illuminate\Database\QueryException $exception) {
                $errorInfo = $exception->errorInfo;
                $result = [
                    'status' => 500,
                    'errorInfo' => $errorInfo,
                    'small_info' => 'Error occured while saving data'
                ];
                echo json_encode($result);
            }
		}
	}

    public function detail(Request $req, $id)
	{
		$resVal = [];
        $head = DB::table('log_gudangs')
        ->join('users', 'log_gudangs.created_by','=', 'users.id')
        ->select('log_gudangs.*', 'users.name')->where('log_gudangs.id', $id)->get();
        $dtl = LogGudangDtl::where('id_log_gudang', $id)->get();
        foreach ($dtl as $i) {
            array_push($resVal, $i);
        }
        return json_encode([
            "data_header" => $head[0],
            "data_detail" => $resVal
        ]);
	}

    public function allItem(Request $req)
	{
		$resVal = [];
        $all_item = LogGudangDtl::join('users', 'log_gudang_dtls.created_by','=', 'users.id')
                    ->select('log_gudang_dtls.*', 'users.name')->orderBy('no_urut', 'asc')->get();
        foreach ($all_item as $i) {
            array_push($resVal, $i);
        }
        return json_encode([
            "data_all" => $resVal
        ]);
	}
}
