let dt_log_gudang, head_data;
$(document).ready(function () {
    $('form').submit(false);
    getData();
    dt_log_gudang = $('#dt_log_gudang').DataTable({
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        order: [[5, 'asc']],
        ajax: {
            url: '/gudang/list',
            type: 'get'
        },
        paging: true,
        searching: true,
        scrollX: true,
        scrollY: "40vh",
        lengthMenu: [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
        columnDefs: [
            {
                targets: 0,
                data: 'id',
                title: 'Id',
                className: 'text-nowrap align-middle',
            },
            {
                targets: 1,
                data: 'jml_masuk',
                title: 'Sum Items',
                className: 'text-nowrap text-right',
                render: function(data, type, full, meta) {
                    return data + ' item';
                }
            },
            {
                targets: 2,
                data: 'current_sum',
                title: 'Total Current Items',
                className: 'text-nowrap text-right',
                render: function(data, type, full, meta) {
                    return data + ' item';
                }
            },
            {
                targets: 3,
                data: 'created_by',
                title: 'Created By',
                className: 'text-nowrap text-center',
                render: function(data, type, full, meta) {
                    return full.name;
                }
            },
            {
                targets: 4,
                data: 'created_at',
                title: 'Input Date',
                className: 'text-nowrap text-right',
                render: function(data, type, full, meta) {
                    return data.substring(0,10);
                }
            },
            {
                targets: 5,
                data: 'id',
                title: 'Option',
                className: 'text-nowrap align-middle',
                render: function(data, type, full, meta) {
                    return '<a href="#" class="btn btn-sm btn-primary btn_detail" data-toggle="modal" data-target="#detailModal"> '+
                        '<i class="fa fa-eye"></i> Detail'+
                        '</a>';
                }
            },
        ]        
    });

    $('#btn_save').on('click', function() {
        let e = document.querySelector("#btn_save");
        let form_input = new FormData($('#form_input')[0]);
        let url = '/gudang/save';
        $.ajax({
            url         : url,
            data        : form_input,
            type        : 'POST',
            dataType    : 'JSON',
            processData : false,
            contentType : false,
            success: function(result){
                if (result.status == 200) {
                    getData();
                    $('#btn_cm').trigger('click');
                    dt_log_gudang.ajax.url('/gudang/list').load();    
                    Swal.fire({
                        title: 'Save data success!',
                        icon: 'success',
                        html: "Log data added : <b>" + result.sum_log_added + "</b><br>" +
                        "Items added : <b>" + result.sum_item_added + "</b><br>" +
                        "Total current items : <b>" + result.current_total_item + "</b>"
                    });                    
                } else if(result.status == 400){
                    Swal.fire({
                        title: 'Failed save data!',
                        icon: 'error',
                        html: result.small_info + "<br>"
                    });
                } else {                    
                    Swal.fire({
                        title: 'Falied save data!',
                        icon: 'error',
                        html: result.small_info + "<br>"+
                        "Space remain : <b>" + result.sisa_space + "</b><br>"                        
                    });
                }                    
            }
        });
    });

    $("#dt_log_gudang").on('click', '.btn_detail', function () {
        let tr = this.closest("tr");
        let trdata = dt_log_gudang.row(tr).data();
        $.ajax({
            url: "/gudang/detail/" + trdata.id,
            type: "GET",
            data: {id: trdata.id},
            dataType: "JSON",
            success: function (result) {
                // Setup data header
                $('.log_id').html(result.data_header.id);
                $('.created_by').html(result.data_header.name);
                $('.sum_items').html(result.data_header.jml_masuk);
                $('.created_at').html(result.data_header.created_at.substring(0,10));
                // Setup html detail data
                var html_table = '';
                html_table +=   '<table id="dt_detail" class="display" style="width:100%">\n'+
                                '   <thead>\n'+
                                '       <tr>\n'+
                                '           <th>Seq Number</th>\n'+
                                '           <th>Location</th>\n'+
                                '           <th>Input Date</th>\n'+
                                '       </tr>\n'+
                                '   </thead>\n'+
                                '   <tbody>\n';
                for (let i = 0; i < result.data_detail.length; i++) {                    
                    let dtData = result.data_detail[i];
                    html_table +=   '       <tr>\n'+
                                    '           <td>' + dtData.no_urut + '</td>\n'+
                                    '           <td> Rack-' + dtData.rak_simpan + ' || Row-' + dtData.baris_ke +
                                    '           </td>\n'+
                                    '           <td>' + dtData.created_at.substring(0,10) + '</td>\n'+
                                    '       </tr>\n';
                }
                html_table +=   '   </tbody>\n'+
                                '</table>';
                $('.modal_dtl_body').html(html_table);
                $('#dt_detail').DataTable();
            }
        });
    });

    $(".btn_all_items").on('click', function () {
        $.ajax({
            url: "/gudang/all-items",
            type: "GET",
            dataType: "JSON",
            success: function (result) {
                var html_table = '';
                html_table +=   '<table id="dt_all_item" class="display" style="width:100%">\n'+
                                '   <thead>\n'+
                                '       <tr>\n'+
                                '           <th>Seq Number</th>\n'+
                                '           <th>Location</th>\n'+
                                '           <th>Created By</th>\n'+
                                '           <th>Input Date</th>\n'+
                                '       </tr>\n'+
                                '   </thead>\n'+
                                '   <tbody>\n';
                for (let i = 0; i < result.data_all.length; i++) {                    
                    let aData = result.data_all[i];
                    html_table +=   '       <tr>\n'+
                                    '           <td>' + aData.no_urut + '</td>\n'+
                                    '           <td> Rack-' + aData.rak_simpan + ' || Row-' + aData.baris_ke +
                                    '           </td>\n'+
                                    '           <td>' + aData.name + '</td>\n'+
                                    '           <td>' + aData.created_at.substring(0,10) + '</td>\n'+
                                    '       </tr>\n';
                }
                html_table +=   '   </tbody>\n'+
                                '</table>';
                $('.modal_all_item').html(html_table);
                $('#dt_all_item').DataTable();
            }
        });
    });

    $("#btn_calculate").on('click', function () {
        var rack_needed, jml_masuk = $('#jml_masuk').val() == '' ? 0 : $('#jml_masuk').val();
        if ( jml_masuk % 51 == 0) {
            rack_needed     = Math.round(jml_masuk / 51);
        } else {
            rack_needed     = Math.round(jml_masuk / 51) + 1;
        }
        Swal.fire({
            title: 'Rack & space needed',
            icon: 'info',
            html: "Rack needed : " + rack_needed + "<br>\n"+
            "Space needed : " + jml_masuk + "<br>"
        });
    });

    $(".btn_add_log").on('click', function () {        
        $('#jml_masuk').val('');
    });
});

function getData() {
    $.ajax({
        url: "/gudang/list",
        type: "GET",
        data: {id: 6},
        dataType: "JSON",
        success: function (result) {
            head_data = result.header;
            // Space data
            $('#total_space').html(head_data.total_space);
            $('#max_capacity').html(head_data.max_capacity);
            $('#space_used').html(head_data.space_used);
            $('#space_remain').html(head_data.space_remain);
            $('#space_unused').html(head_data.space_unused);
            // Rack data
            $('#total_rack').html(head_data.total_rack);
            $('#rack_used').html(head_data.rack_used);
            $('#rack_remain').html(head_data.rack_remain);            
        }
    });    
}